#Criação de pastas e movimentação de arquivos
#_________________________________________________________________________________________________________________________________
#Seção 1: bibliotecas

# A biblioteca glob percorre a pasta e cria uma lista dos arquivos existentes
import glob

# A biblioteca os nos permite utilizar funções do sistema operacional
import os

# A biblioteca shutil permite a movimentação de arquivos pelas pastas
import shutil

# Seção 2: identificação dos arquivos

# Aqui dizemos ao script onde estão os arquivos que queremos manipular e qual o tipo de extensão

# Exemplo:
#arquivos = glob.glob("/home/roganti/Imagens/*.jpeg")
# O final /*.jpeg percorre todos os arquivos da extensão jpeg
arquivos = glob.glob("")

# Seção 3: listagem de arquivos

# Aqui fazemos uma lista dos arquivos encontrados na seção anterior e percorremos para prosseguir com a manipulação
for i in arquivos:
x = i.split("/") # Utilizamos o ("/") para retirar o caminho dos arquivos e deixar somente os nomes
y = x[4].split("-") # Aqui utilizamos o ("-") para separar o nome da pessoa do numero da foto. Gabriel-1, Gabriel-2 por ex.
caminho = x[4]
nome = str(y[0])
# Aqui utilizamos o nome dos arquivos da lista para gerar uma pasta com o mesmo nome. Inserimos também o caminho para criar

# Exemplo:
#pasta =str( '/home/roganti/pastaGerada/%s' %nome)
pasta =str(  %nome)

# Seção 4: Criação da pasta, movimentação do arquivo e tratamento de erros
try:
    os.mkdir(pasta) # Aqui é criada uma pasta com o nome do arquivo listado

    # Aqui movemos o arquivo para a pasta de mesmo nome
    # Exemplo:
    # shutil.move('/home/roganti/Imagens/%s' %caminho, '/home/roganti/pastaGerada/%s/%s' %(nome,caminho))
    # Aqui não apaguei os caminhos utilizados para não gerar confusão, mas basta substituir os caminhos de pasta
    shutil.move('/home/roganti/Imagens/%s' %caminho, '/home/roganti/pastaGerada/%s/%s' %(nome,caminho))
# Aqui identificamos os erros e tratamos. Se a pasta já existe, simplesmente move o arquivo
except OSError:
    print ("Envio para a pasta %s Já criada" % pasta)
    shutil.move('/home/roganti/Imagens/%s' %caminho, '/home/roganti/pastaGerada/%s/%s' %(nome,caminho))
# Aqui informamos que a pasta foi criada
else:
    print ("Foi criada com sucesso a pasta %s " % pasta)
